package id.sch.smktelkom_mlg.tugas01.xirpl208.bungatunggal;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText etmodal;
    EditText etbunga;
    EditText etwaktu;
    Button proses;
    TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etmodal = findViewById(R.id.modal);
        etbunga = findViewById(R.id.bunga);
        etwaktu = findViewById(R.id.waktu);
        hasil = findViewById(R.id.txthasil);
        proses = findViewById(R.id.btnproses);

        proses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int modal = Integer.parseInt(etmodal.getText().toString());
                double bunga = Double.parseDouble(etbunga.getText().toString());
                int waktu = Integer.parseInt(etwaktu.getText().toString());

                double hitung = modal * bunga / 100 * waktu;
                hasil.setText(String.valueOf("Dengan modal sebesar Rp." + modal + "dan bunga sebesar "
                        + bunga + "% Anda memiliki Bunga RP " + hitung + ",0"));
            }
        });
    }
}
