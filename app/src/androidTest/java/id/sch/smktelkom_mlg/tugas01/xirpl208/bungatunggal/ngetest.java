package id.sch.smktelkom_mlg.tugas01.xirpl208.bungatunggal;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;


/**
 * Created by aliya nugraha on 1/25/2018.
 */

public class ngetest {
    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void clickbtnproses() {
        Espresso.onView(ViewMatchers.withId(R.id.modal)).perform(ViewActions.typeText("2000000"));
        Espresso.onView(ViewMatchers.withId(R.id.bunga)).perform(ViewActions.typeText("10"));
        Espresso.onView(ViewMatchers.withId(R.id.waktu)).perform(ViewActions.typeText("2"));
        Espresso.onView(ViewMatchers.withId(R.id.btnproses)).perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withId(R.id.txthasil)).check(ViewAssertions.matches(ViewMatchers.withText
                ("Dengan modal sebesar Rp.2000000dan bunga sebesar 10.0% Anda memiliki Bunga RP 400000.0,0")));

    }
}
